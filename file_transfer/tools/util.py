from datetime import datetime
import math


def get_mjd_datetime(date: datetime) -> int:
    """
    Calculate MJD day from datetime
    :param date: datetime object to convert to MJD
    :return: MJD day
    """
    # Ensure correct format
    if not isinstance(date, datetime):
        raise TypeError('Invalid type for parameter "date" - expecting datetime')
    elif date.year < 1801 or date.year > 2099:
        raise ValueError('Datetime must be between year 1801 and 2099')

    # Perform the calculation
    julian_datetime = 367 * date.year - \
                      int((7 * (date.year + int((date.month + 9) / 12.0))) / 4.0) + \
                      int((275 * date.month) / 9.0) + \
                      date.day + 1721013.5 + \
                      (date.hour + date.minute / 60.0 + date.second / math.pow(60, 2)) / 24.0 - \
                      0.5 * math.copysign(1, 100 * date.year + date.month - 190002.5) + 0.5

    return int(julian_datetime - 2400000.5)


def set_attributes(obj, **kwargs) -> None:
    """
    Setter for class attributes, takes attribute names and values from kwargs.
    If there is no corresponding attribute in class description it does nothing, otherwise it sets attribute value.
    !!! IMPORTANT !!! If used in constructor, attributes of the class must pe initialized before function call !!!
    :param obj: class instance
    :param kwargs: attribute names and values
    :return: None
    """
    for key, value in kwargs.items():
        if key in obj.__dict__.keys():
            setattr(obj, key, value)

from time import sleep
from typing import Type, Union, Tuple
import functools


def repeat_on_error_decorator(count: int = 5, timeout: int = 5,
                              exc: Union[Tuple[Type[Exception]], Type[Exception]] = Exception):
    """
    Decorator to wrap functions that can unexpectedly fail and need to retry
    :param count: count of functions repeat
    :param timeout: seconds between repeating
    :param exc: tuple or single exceptions to catch
    :return:
    """
    def actual_decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            result = None
            for i in range(count):
                try:
                    return func(*args, **kwargs)
                except exc:
                    if i + 1 == count:
                        raise TimeoutError('failed operation ' + func.__name__)
                sleep(timeout)
            return result
        return wrapper
    return actual_decorator

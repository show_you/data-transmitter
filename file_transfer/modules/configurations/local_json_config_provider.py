from file_transfer.modules.configurations.configurations_provider import ConfigurationsProvider
import json


class LocalJsonConfigProvider(ConfigurationsProvider):
    """
    Configurations provider, that loads json file with configurations from local filesystem
    """
    config_path: str
    configurations: list[dict] = [{}]
    loaded: bool = False

    def __init__(self, path: str):
        super(ConfigurationsProvider, self).__init__()
        self.config_path = path

    def load_config(self) -> None:
        with open(self.config_path, 'r') as config_file:
            config = json.load(config_file)['tasks']
            if not isinstance(config, list) or not isinstance(config[0], dict):
                raise KeyError(f"Incorrect config structure of file {self.config_path}")
            self.configurations = config
        self.loaded = True

    def get_tasks_config_list(self) -> list[dict]:
        if not self.loaded:
            self.load_config()
        return self.configurations

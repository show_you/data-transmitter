

class ConfigurationsProvider:
    """
    Base abstract class for configurations parsing
    """

    def load_config(self) -> None:
        """
        Load and prepare config before use
        :return: None
        """
        raise NotImplementedError()

    def get_tasks_config_list(self) -> list[dict]:
        """
        Get the list with tasks configurations
        :return: list with configurations represented as dictionaries for each task to execute
        """
        raise NotImplementedError()

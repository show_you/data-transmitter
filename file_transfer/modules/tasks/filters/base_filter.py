

class BaseFilter:
    """
    Base class for data filter functionality.
    """
    def filter(self, parameter: str) -> bool:
        """
        Filter function.
        :param parameter: parameter to filter by
        :return: True if data is allowed, else False
        """
        # Allow anything
        return True

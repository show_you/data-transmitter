from file_transfer.modules.tasks.filters.base_filter import BaseFilter
from file_transfer.modules.tasks.formatters.base_formatter import BaseFormatter
from file_transfer.modules.tasks.modifiers.base_modifier import BaseModifier
from file_transfer.modules.tasks.sources.base_source import BaseSource


class TransferTask:
    """
    Base abstract transfer task class
    """

    name: str
    source: BaseSource
    source_filter: BaseFilter
    source_formatter: BaseFormatter
    modifiers: list[BaseModifier]
    destination_formatter: BaseFormatter
    destination: BaseSource

    def __init__(self, **kwargs):
        self.name = kwargs['name']
        self.source = globals()[kwargs['source']['class']](**kwargs['source'])

        if 'filter' in kwargs and 'class' in kwargs['filter'] \
                and len(kwargs['filter']['class']):
            self.source_filter = globals()[kwargs['filter']['class']](**kwargs['filter'])
        else:
            self.source_filter = BaseFilter()

        if 'source_formatter' in kwargs and 'class' in kwargs['source_formatter'] \
                and len(kwargs['source_formatter']['class']):
            self.source_formatter = globals()[kwargs['source_formatter']['class']](**kwargs['source_formatter'])
        else:
            self.source_formatter = BaseFormatter()

        self.modifiers = []
        if 'data_modifiers' in kwargs and len(kwargs['data_modifiers']):
            for data_modifier in kwargs['data_modifiers']:
                if 'class' in data_modifier and len(data_modifier['class']):
                    self.modifiers.append(globals()[data_modifier['class']](**data_modifier))

        if 'destination_formatter' in kwargs and 'class' in kwargs['destination_formatter'] \
                and len(kwargs['destination_formatter']['class']):
            self.destination_formatter = \
                globals()[kwargs['destination_formatter']['class']](**kwargs['destination_formatter'])
        else:
            self.destination_formatter = BaseFormatter()

        self.destination = globals()[kwargs['destination']['class']](**kwargs['destination'])

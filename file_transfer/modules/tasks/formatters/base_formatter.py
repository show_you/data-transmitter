

class BaseFormatter:
    """
    Base data formatter class
    """
    def apply(self, base_objects: list[str]) -> list[str]:
        """
        Change base objects format.
        :param base_objects: list of string representations of data objects
        :return: list of new data objects after format
        """
        # Don't format
        return base_objects
